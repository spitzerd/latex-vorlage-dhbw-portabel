# Vorlage für das wissenschaftliche Arbeiten an der DHBW - portable Version
Die ZIP-Datei in diesem Repository enthält eine portable Version von MikTeX, Texmaker und zwei LaTeX-Vorlagen. Diese LaTeX-Vorlagen wurden für das wissenschaftliche Arbeiten an der DHBW Lörrach am SZI beziehungsweise am DHBW CAS entwickelt.
Man kann die ZIP-Datei auch auf einen USB-Stick entpacken. Somit kann man seine LaTeX-Arbeit mitnehmen und mehreren Computern an seiner Arbeit schreiben. Admin-Rechte auf dem Computer sind nicht notwendig.
Die Programme aus der ZIP-Datei funktionieren nur unter Microsoft Windows.

Um Speicherplatz zu sparen, werden ältere Commits in diesem Repository mit Absicht überschrieben. Den Verlauf der Entwicklung der Vorlagen lässt sich in den jeweiligen Repositories nachvollziehen, welche unten verlinkt sind.

## Anleitung
1. [ZIP-Datei](https://gitlab.com/spitzerd/latex-vorlage-dhbw-portabel/-/raw/main/latex-dhbw-portabel.zip) herunterladen.
2. ZIP-Datei entpacken. Mit dem Programm 7zip geht das Entpacken schneller. 
3. Doppelklick auf `Start Texmaker.cmd` um Texmaker zu starten.
4. Die Datei `document.tex` mit Texmaker öffnen. Diese liegt im Ordner `latex-vorlage-dhbw-cas` bzw. `latex-vorlage-dhbw-loerrach-szi`
5. Mit einem Klick auf den Pfeil links neben "Schnelles Übersetzen" in Texmaker wird ein PDF-Dokument erzeugt. Dieser Prozess dauert etwa 20 Sekunden.

MikTeX kann bei Bedarf gestartet werden, zum Beispiel wenn man Einstellungen in MikTeX ändern möchte. Es ist nicht nötig während dem Schreiben MikTeX geöffnet zu haben. MikTeX ist nach dem Öffnen versteckt in der Windows-Taskleiste.

## Ordner
Wenn Ordner umbenannt, verschoben oder gelöscht werden, kann es zu Fehlermeldungen kommen. Möchte man die Ordner verschieben oder umbenennen, dann sollte man an folgenden Stellen die Pfade gegebenenfalls ändern:
- `latex-vorlage-dhbw-*/.vscode/settings.json`
- In Texmaker unter `Optionen --> Texmaker konfigurieren --> Befehle --> "Zur PATH Variable hinzufügen"`
- Die beiden .CMD-Dateien im Wurzelverzeichnis

## Schreiben mit Visual Studio Code
Alternativ zu Texmaker kann man auch mit Visual Studio Code seine Arbeit schreiben. VS Code ist wie Texmaker portabel und lässt sich auf einem USB-Stick ohne Admin-Recht installieren. 
1. VS Code als ZIP-Datei herunterladen und entpacken: https://code.visualstudio.com/download
2. In den entpackten Ordner einen neuen Ordner anlegen und diesen `data` nennen. Eine ausführliche Anleitung ist hier verfügbar: https://code.visualstudio.com/docs/editor/portable
3. Die VS Code Erweiterung "[LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)" installieren.
4. Die VS Code Erweiterung "[LTeX+](https://marketplace.visualstudio.com/items?itemName=ltex-plus.vscode-ltex-plus)" für die Rechtschreibprüfung installieren.
5. Die LaTex-Vorlage öffnen: `Datei -> Ordner öffnen...`

Weitere Infos zum Schreiben mit VS Code sind hier verfügbar: https://gitlab.com/spitzerd/latex-vorlage-dhbw-cas/-/blob/master/manuals.md

## Links
- MikTeX: https://miktex.org/
- Texmaker: https://www.xm1math.net/texmaker/ 
- LaTeX-Vorlage für das DHBW CAS: https://gitlab.com/spitzerd/latex-vorlage-dhbw-cas
- LaTeX-Vorlage für das SZI an der DHBW Lörrach: https://gitlab.com/spitzerd/latex-vorlage-dhbw-loerrach-szi
